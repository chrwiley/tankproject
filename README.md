created all the different states 

Tank 1 is set to Chase
Tank 2 is set to Chase and Fire
Tank 3 is set to Flee
Tank 4 is set to Chase
          
    void DoChase()
    {
        motor.RotateTowards(target.position, data.turnLeftSpeed);

        if (CanMove(data.moveForwardSpeed)) //can we move moveForwardSpeed units away (this is actually looking for collisions
        {
            motor.Move(data.moveForwardSpeed);
        }
        else
        {
            avoidanceStage = 1; //enters the avoidance stage
        }
    }

    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            motor.Rotate(-1 * data.turnLeftSpeed);

            if (CanMove(data.moveForwardSpeed)) //if tank can move go to stage 2
            {
                avoidanceStage = 2;

                exitTime = data.avoidanceTime; //how long will we stay in avoidance
            }

            //Keep going
        }
        else if (avoidanceStage == 2)
        {
            if (CanMove(data.moveForwardSpeed))  //if we can move do it 
            {
                exitTime -= Time.deltaTime;  //counting down
                motor.Move(data.moveForwardSpeed);

                if (exitTime <= 0) //is there avoidance time left
                {
                    avoidanceStage = 0; //return to chase stage
                }
            }
            else
            {
                avoidanceStage = 1; //can't move go back to stage 1
            }
        }
    }

   public void CheckForFlee()
    {
        ChangeState(AIState.Flee);
    }

    public void DoRest()
    {
        thealth.currentHealth += data.restingHealRate * Time.deltaTime;  //increase health per second

        thealth.currentHealth = Mathf.Min(thealth.currentHealth, 100);  //when I used data.maxHealth I got an error

    }

    public void ChangeState(AIState newState)
    {
        aiState = newState;  //change state
        data.stateEnterTime = Time.time;  //what time did we change state
    }

    public void DoFlee()
    {
        Vector3 vectorToTarget = target.position - tf.position;  //target position - ai position

        Vector3 vectorAwayFromTarget = -1 * vectorToTarget; //flip by -1 and move away from target 

        vectorAwayFromTarget.Normalize();  //give it a magnitude of 1 

        vectorAwayFromTarget *= data.fleeDistance;   //multiply by itself to give a vector

        Vector3 fleePosition = vectorAwayFromTarget + tf.position;  //position away from target
        motor.RotateTowards(fleePosition, data.turnLeftSpeed);
        motor.Move(data.moveForwardSpeed);
    }
}






